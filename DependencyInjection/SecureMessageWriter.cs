﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DependencyInjection
{
    public class SecureMessageWriter : IMessageWriter
    {
        private readonly IMessageWriter Writer;
        public SecureMessageWriter(IMessageWriter Writer)
        {
            if(Writer==null)
            {
                throw new NullReferenceException();
            }

            this.Writer = Writer;
        }

        public void Write(string Message)
        {
            if(Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                Writer.Write("HelloDI");
            }
        }
    }
}
