﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    class Salutaion
    {
        private readonly IMessageWriter Writer;
        public Salutaion(IMessageWriter Writer)
        {
            if(Writer==null)
            {
                throw new ArgumentNullException("Writer");
            }
            this.Writer = Writer;
        }
        public void Exclaim()
        {
            this.Writer.Write("Hello DI");
        }
    }
}
