﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DependencyInjection
{
    class ConsoleMessage
    {
        public static void Main()
        {
            //IMessageWriter Writer = new ConsoleMessageWriter();
            //var typeName = ConfigurationManager.AppSettings[""];
            //var type = Type.GetType(typeName, true);
            //IMessageWriter Writer = (IMessageWriter)Activator.CreateInstance(type);


            IMessageWriter Writer = new SecureMessageWriter(new ConsoleMessageWriter());
            var salutation = new Salutaion(Writer);
            salutation.Exclaim();
        }
    }
}
