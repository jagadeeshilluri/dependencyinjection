﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public class ConsoleMessageWriter : IMessageWriter
    {
        public void Write(string Message)
        {
            Console.WriteLine(Message);
            Console.ReadLine();
        }
    }
}
